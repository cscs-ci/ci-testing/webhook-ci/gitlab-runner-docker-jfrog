Gitlab runner that creates a new docker image and pushes the artifact to jfrog.svc.cscs.ch (container registry)

This runner will build a new container and push the image to the container registry. There are 2 modes of operation:
1. **Dockerfile mode** (*preferred mode*): You need to point the variable `DOCKERFILE` to a Dockerfile which is used as recipe to build the new container, i.e..
    ```yaml
    variables:
      DOCKERFILE: ci/docker/Dockerfile.build
    ```
    - This mode is preferred because local testing is possible by issuing `docker build -f ci/docker/Dockerfile.build .`

2. **Script mode**: The base image is specified in the tag `image` and the commands that are run inside the container are specified in the `script` tag:
    ```yaml
    image: ubuntu:22.04
    script:
        - apt-get update
        - env DEBIAN_FRONTEND=noninteractive TZ=Europe/Zurich apt-get -yqq install --no-install-recommends build-essential cmake
        - ...
    ```

---

# Supported variables:
* **DOCKERFILE**
  - Path in the repository to the Dockerfile with build instructions (it starts at the root of the repository, but it should NOT start with a `/`)
* **PERSIST_IMAGE_NAME**
  - This is the tag name that will be used to push it to the container registry
  - It must start with `$CSCS_REGISTRY_PATH`
  - You probably want to add a version number to it, i.e. the part after the colon
* **DOCKER_BUILD_ARGS**
  - JSON-array string with arguments that should be passed to `docker build` as `--build-arg`
  
Example:
<div><code>.gitlab-ci.yml</code></div>

```yaml
include:
  - remote: 'https://gitlab.com/cscs-ci/recipes/-/raw/master/templates/v2/.ci-ext.yml'
stages:
  - build
build_job:
  stage: build
  extends: .container-builder
  variables:
    DOCKERFILE: ci/docker/Dockerfile.build
    PERSIST_IMAGE_NAME: $CSCS_REGISTRY_PATH/my_image_name:$CI_COMMIT_REF_NAME
    DOCKER_BUILD_ARGS: '["ARG1=value1", "OtherArgName=AnotherValue"]'
```
