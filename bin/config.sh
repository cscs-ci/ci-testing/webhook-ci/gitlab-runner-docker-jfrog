#!/usr/bin/env bash

cat << EOF
{
  "builds_dir": "/tmp/gitlab-runner/docker-jfrog/builds/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "cache_dir": "/tmp/gitlab-runner/docker-jfrog/cache/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "builds_dir_is_shared": false,
  "driver": {
    "name": "Docker+JFrog runner",
    "version": "v0.1"
  },
  "job_env": {
    "CUSTOM_TMP_DIR": "/tmp/gitlabrunner-docker-jfrog/${CUSTOM_ENV_CI_PIPELINE_ID}_${CUSTOM_ENV_CI_JOB_ID}"
  }
}
EOF
