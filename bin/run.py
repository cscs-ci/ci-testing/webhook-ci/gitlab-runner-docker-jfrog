#!/usr/bin/env python

import logging
from logging.handlers import RotatingFileHandler

from typing import Dict, Optional

import copy
import json
import os
import re
import shutil
import subprocess
import sys

logfile = '/tmp/gitlabrunner-docker-jfrog.log'
logger = logging.getLogger("docker_jfrog")


temporary_dir = '/tmp/gitlabrunner-docker-jfrog'
runner_env: Dict[str, str] = {}

artifactory_dir = 'contbuild/testing/anfink'

class NoStackInfoLogger(logging.Formatter):
    def __init__(self, fmt: Optional[str]=None, datefmt: Optional[str]=None, style: str='%'):
        super().__init__(fmt, datefmt, style)

    def format(self, record: logging.LogRecord) -> str:
        stack_info = record.stack_info
        record.stack_info = None
        ret = super().format(record)
        record.stack_info = stack_info
        return ret


def setupLogger() -> None:
    # setup logger
    deflogger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    deflogger.setLevel(logging.INFO)   # when setting this to DEBUG, then remove fileRotationHandler from deflogger
    fileRotationHandler = RotatingFileHandler(logfile, maxBytes=20*1024*1024, backupCount=10)
    fileRotationHandler.setLevel(logging.DEBUG)
    loggerch = logging.StreamHandler()
    loggerch.setLevel(logging.WARNING)
    loggerformatter = NoStackInfoLogger('%(asctime)s - %(levelname)s - %(message)s', datefmt="%H:%M:%S")
    verbose_logger_formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(name)s - %(threadName)s - %(filename)s:%(funcName)s:%(lineno)s - %(message)s", datefmt="%Y-%m-%d-%H:%M:%S")
    fileRotationHandler.setFormatter(verbose_logger_formatter)
    loggerch.setFormatter(loggerformatter)
    deflogger.addHandler(loggerch)
    deflogger.addHandler(fileRotationHandler)  # events propagate in the hierarchy, i.e. we do not need to attach the handler to the docker_jfrog-logger if we attach it to the deflogger (root)


def tmpDir() -> str:
    return os.environ['CUSTOM_TMP_DIR']
    return os.path.join(temporary_dir, f"{runner_env['CI_PIPELINE_ID']}_{runner_env['CI_JOB_ID']}")

# returns the path to the generated dockerfile
def generateDockerfile(mount_craylic: bool=False) -> str:
    dockerfilepath = os.path.join(tmpDir(), 'Dockerfile_'+runner_env['CI_JOB_ID'])
    with open(dockerfilepath, 'w') as dockerfile:
        dockerfile.write(f'FROM {runner_env["CI_JOB_IMAGE"]}\n')
        dockerfile.write(f'WORKDIR /sources\n')
        dockerfile.write(f'COPY . .\n')
        licfile_mount = '' if not mount_craylic else '--mount=type=secret,id=craylic,target=/opt/cray/pe/craype/2.7.9/AutoPass/Lic/CPE-licfile.dat bash -l -c'
        dockerfile.write(f'RUN {licfile_mount} "./{runner_env["CI_PIPELINE_ID"]}_{runner_env["CI_JOB_ID"]}_buildscript.sh"\n')
#        dockerfile.write(f'RUN cd / && rm -Rf {runner_env["CI_PROJECT_DIR"]}\n')
    return dockerfilepath

def allow_craylic_secret(project_id: str) -> bool:
    jdata = json.load(open('/home/anfink/ci_test/config.json'))
    if project_id in jdata and jdata[project_id].get('allow_craylic', False):
        return True
    return False



if __name__ == '__main__':
    setupLogger()
    logger.debug(f'sys.argv={sys.argv} env={os.environ}')

    # gitlab runner variables are passed via the environment, but the variable names start with 'CUSTOM_ENV_', we create a variable that collects all CUSTOM_ENV_ variables and remove the CUSTOM_ENV_,
    # i.e. they can be accessed as provided in the gitlab documentation
    runner_env = { k[11:]: v for k,v in os.environ.items() if k.startswith('CUSTOM_ENV_') }

    if sys.argv[-1] == 'build_script' or sys.argv[-1] == 'step_script':
        # This is the step as provided in the .gitlab-ci.yml under the script-tag
        os.makedirs(tmpDir(), exist_ok=True, mode=0o700)

        project_id_match = re.match(r'.*-(\d+)$', runner_env['CI_PROJECT_NAME'])
        with_craylic =  project_id_match is not None and project_id_match.lastindex==1  and allow_craylic_secret(project_id_match[1])

        dockerfile = runner_env.get('DOCKERFILE', 'generate')
        if dockerfile.startswith('/'): dockerfile = dockerfile[1:]
        if 'generate' == dockerfile:
            dockerfile = generateDockerfile(with_craylic)
            new_buildscript = open(sys.argv[-2]).read().replace(runner_env['CI_PROJECT_DIR'], '/sources')
            new_buildscript_path = f'{runner_env["CI_PROJECT_DIR"]}/{runner_env["CI_PIPELINE_ID"]}_{runner_env["CI_JOB_ID"]}_buildscript.sh'
            with open(new_buildscript_path, 'w') as buildscript:
                buildscript.write(new_buildscript)
            os.chmod(new_buildscript_path, 0o755)
        else:
            dockerfile = os.path.join(runner_env["CI_PROJECT_DIR"], dockerfile)

        docker_tag = f'{runner_env["CSCS_REGISTRY"]}/{artifactory_dir}/{runner_env["PERSIST_IMAGE_NAME"] if "PERSIST_IMAGE_NAME" in runner_env else runner_env["CI_PROJECT_NAME"]+":1.0"}'
        logger.info(f'Removing docker image with tag={docker_tag}')
        subprocess.run(['docker', 'rmi', docker_tag], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL) # delete image if it exists with this tag (do not check returncode, this can fail!)
        subprocess.run(['sudo', '/root/bin/connect_vpn'], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL) # make sure VPN is up and running (do not check returncode, this can fail, if it is already running)
        logger.info(f'Building docker image from dockerfile {dockerfile}')
        build_env = copy.deepcopy(os.environ)
        additional_args = []
        # add craylic secret only for whitelisted projects
        if with_craylic:
            additional_args.extend(['--secret', 'id=craylic,src=/home/anfink/cpe/CPE-licfile.dat'])
            build_env['DOCKER_BUILDKIT'] = '1'
        logger.info(f'DOCKER_BUILDKIT={build_env.get("DOCKER_BUILDKIT", 0)}')
        subprocess.run(['docker', 'build', '-f', dockerfile, '--force-rm', '--pull=true', '-t', docker_tag, runner_env["CI_PROJECT_DIR"], *additional_args], env=build_env).check_returncode()
        if 'PERSIST_IMAGE_NAME' in runner_env:
            logger.info(f'Logging into docker registry {runner_env["CSCS_REGISTRY"]}')
            subprocess.run(['docker', 'login', runner_env["CSCS_REGISTRY"]], input=f'{runner_env["CSCS_REGISTRY_USER"]}\n{runner_env["CSCS_REGISTRY_PASSWORD"]}'.encode('utf-8')).check_returncode()
            logger.info(f'Pushing image with tag={docker_tag}')
            subprocess.run(['docker', 'push', docker_tag]).check_returncode()
        subprocess.run(['docker', 'rmi', docker_tag], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL) # delete image if it exists with this tag (do not check returncode, this can fail!)
    elif sys.argv[-1] == 'after_script':
        logger.warning(f'after_script is ignored with this runner. No command will be executed. If this feature is required please contact us with your use case.')
    else:
        # run script as created by the gitlab runner
        subprocess.run(sys.argv[-2]).check_returncode()

